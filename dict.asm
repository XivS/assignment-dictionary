global find_word
global get_tuple_by_element

extern string_length
extern string_equals

%define DQ_SHIFT 8
; input: 
; 	rdi - pointer to element 
;  output: 
; 	rax - key (address)
;	rdx - value (address)
; 

get_tuple_by_element:
	xor rax, rax
	xor rdx, rdx
	mov r8, [rdi]
	add rdi, DQ_SHIFT
	call string_length
	lea rdx, [rdi+rax+1]
	mov rax, rdi
	ret


;input:
;	rdi - pointer to dict
;	rsi - pointer to string
;output:
;	rax:
;		0 - wor not found
;		not 0 - addr of element	

find_word:
	
	.lp:
		push rdi
		call get_tuple_by_element
		mov rdi, rax
		call string_equals
		pop rdi
		test rax, rax
	    jnz .found
		mov rdi, [rdi]
		test rdi, rdi
		jz .not_found
		jmp .lp
		
		
	.found:
		mov rax, rdi
		ret
	.not_found:
		xor rax, rax
		ret
		
		
	