%include "colon.inc"
global _start
extern exit
extern print_string
extern find_word
extern print_char
extern get_tuple_by_element
extern check_whitespace
extern string_length

%define ASCII_NUM_TO_CHAR_SHIFT 0x30
%define STRING_BUFF_SIZE 6
%define ASCII_NEWLINE 0xA

section .rodata
buff_overflow_err_msg: db 27,"[31;1mFatal: string buffer overflow!",0

section .data
string_buffer: times STRING_BUFF_SIZE db 0
%include 'words.inc'

section .text

print_stderr:
	call string_length 
	mov rdx, rax 
	mov rsi, rdi 
	mov rdi, 2
	mov rax, 1 
	syscall 
    ret

	

read_string_to_buffer:
	xor rax, rax
	mov rsi, string_buffer
	mov rdi, 0
	mov rdx, STRING_BUFF_SIZE
	syscall
	mov rdi, [string_buffer + rax - 1]
	cmp dil, ASCII_NEWLINE
	je .enter_correction
	cmp byte [string_buffer + STRING_BUFF_SIZE - 1], 0
	jne .overflow
	ret
	.enter_correction:
		mov byte [string_buffer + rax - 1], 0
		ret
	.overflow:
		mov rdi, buff_overflow_err_msg
		call print_stderr
		call exit
		
	
_start:
	call read_string_to_buffer
	;mov rdi, string_buffer
	;call print_string
	mov rdi, f5
	mov rsi, string_buffer
	call find_word
	mov rdi, rax 
	test rdi, rdi
	jz .not_found
	call get_tuple_by_element
	mov rdi, rdx
	call print_string
	call exit
	.not_found:
		mov rdi, ASCII_NUM_TO_CHAR_SHIFT
		call print_char
	call exit
	
