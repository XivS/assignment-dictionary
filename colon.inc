%macro colon 2
	%ifid %2	
		%ifdef PREV_ELEM_POINTER
			%2: dq PREV_ELEM_POINTER
		%else
			%2: dq 0
		%endif
		%define PREV_ELEM_POINTER %2
	%else
		%fatal "2-nd argument contains symbols not supported in labels"
	%endif
	%ifstr %1
		db %1, 0
	%else
		%fatal "1-st argument only supports string values"
		
	%endif
%endmacro